(function(){

  console.log('custom.js');
  console.log({
    '$': $
  });

  let $body = $('body');
  $(window).scroll(function(){
    let scrollTop = $(window).scrollTop();
    if( scrollTop > 50 ) {
      $body.addClass('scrolled');
    } else {
      $body.removeClass('scrolled');
    }
  });

}());